// Put your applicaiton javascript here

// Mavigation smoothscroll
$(document).ready(function(){
   $('#navigation a').on('click', function (e) {
       if (this.hash !== '') {
           e.preventDefault();
           const hash = this.hash;
           $('.toggler').prop("checked", false);
           $('html, body').animate(
               {
                   scrollTop: $(hash).offset().top - 50,
               },
               800
           );
       }
   });
});